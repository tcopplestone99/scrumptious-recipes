from django.contrib import admin
from .models import Recipe

class RecipeAdmin(admin.ModelAdmin):
    pass
# Register your models here.
admin.site.register(Recipe, RecipeAdmin)
